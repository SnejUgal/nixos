{
  outputs = { self }: {
    templates = {
      haskell = {
        path = ./shells/haskell;
        description = "A Haskell dev shell";
      };
      latex = {
        path = ./shells/latex;
        description = "A LaTeX dev shell";
      };
      python = {
        path = ./shells/python;
        description = "A Python dev shell";
      };
      rust = {
        path = ./shells/rust;
        description = "A Rust dev shell";
      };
    };
  };
}

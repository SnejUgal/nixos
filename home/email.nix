{ ... }:

{
  accounts.email.accounts = {
    "snejugal.ru" = {
      primary = true;
      address = "contact@snejugal.ru";
      flavor = "yandex.com";
      realName = "Артем Стариков";
      thunderbird.enable = true;
    };

    IU = {
      address = "a.starikov@innopolis.university";
      userName = "a.starikov@innopolis.university";
      realName = "Artem Starikov";

      thunderbird.enable = true;
      imap = {
        host = "mail.innopolis.ru";
        port = 993;
      };
      smtp = {
        host = "mail.innopolis.university";
        port = 587;
      };
    };

    Invian = {
      address = "artem.starikov@invian.ru";
      flavor = "yandex.com";
      realName = "Артем Стариков";
      thunderbird.enable = true;
    };

    Gmail = {
      address = "starikov.artem03@gmail.com";
      flavor = "gmail.com";
      realName = "Artem Starikov";

      thunderbird = {
        enable = true;
        settings = id: {
          "mail.smtpserver.smtp_${id}.authMethod" = 10;
          "mail.server.server_${id}.authMethod" = 10;
        };
      };
    };

    Yandex = {
      address = "starikov.artem03@yandex.ru";
      userName = "starikov.artem03";
      realName = "Артем Стариков";

      thunderbird.enable = true;
      imap = {
        host = "imap.yandex.ru";
        port = 993;
      };
      smtp = {
        host = "smtp.yandex.ru";
        port = 465;
      };
    };

    iCloud = {
      address = "snejugal@icloud.com";
      userName = "snejugal@icloud.com";
      realName = "Artem Starikov";

      thunderbird.enable = true;
      imap = {
        host = "imap.mail.me.com";
        port = 993;
      };
      smtp = {
        host = "smtp.mail.me.com";
        port = 465;
      };
    };

    Hotmail = {
      address = "starikov.artem03@hotmail.com";
      flavor = "outlook.office365.com";
      realName = "Artem Starikov";
      thunderbird.enable = true;
    };

    "starikov.artem03@mail.ru" = {
      address = "starikov.artem03@mail.ru";
      userName = "starikov.artem03@mail.ru";
      realName = "Артем Стариков";

      thunderbird.enable = true;
      imap = {
        host = "imap.mail.ru";
        port = 993;
      };
      smtp = {
        host = "smtp.mail.ru";
        port = 465;
      };
    };

    "starikovartem03@mail.ru" = {
      address = "starikovartem03@mail.ru";
      userName = "starikovartem03@mail.ru";
      realName = "Артем Стариков";

      thunderbird.enable = true;
      imap = {
        host = "imap.mail.ru";
        port = 993;
      };
      smtp = {
        host = "smtp.mail.ru";
        port = 465;
      };
    };
  };
}

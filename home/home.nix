{ ... }:

{
  imports = [
    ./backup.nix
    ./email.nix
    ./fonts.nix
    ./software.nix
  ];

  home = {
    username = "snejugal";
    homeDirectory = "/home/snejugal";
    stateVersion = "23.11";

    sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };
  };

  systemd.user.tmpfiles.rules = [
    "d %h/Downloads - - - 5d -"
  ];
}

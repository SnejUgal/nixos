{ pkgs, lib, ... }:

{
  imports = [
    ./software/alacritty.nix
    ./software/bash.nix
    ./software/direnv.nix
    ./software/git.nix
    ./software/gpg.nix
    ./software/neovim.nix
    ./software/thunderbird.nix
    ./software/tmux.nix
  ];

  programs = {
    home-manager.enable = true;
    firefox.enable = true;
  };

  home.packages = [
    pkgs.quickemu
    pkgs.tokei
    pkgs.wl-clipboard-rs

    pkgs.anytype
    pkgs.authenticator
    pkgs.galaxy-buds-client
    pkgs.gimp
    pkgs.kdePackages.gwenview
    pkgs.krita
    pkgs.libreoffice
    pkgs.kdePackages.okular
    pkgs.polychromatic
    pkgs.qbittorrent
    pkgs.remmina
    pkgs.telegram-desktop
    pkgs.vlc

    pkgs.lutris
    pkgs.superTuxKart
    pkgs.mindustry
  ];

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "anytype"
    "anytype-heart"
    "steam"
    "steam-unwrapped"
  ];
}

{ ... }:

{
  programs.git = {
    enable = true;
    userEmail = "contact@snejugal.ru";
    userName = "Artem Starikov";
    signing = {
      key = null;
      signByDefault = true;
    };
    extraConfig = {
      push.autoSetupRemote = true;
      init.defaultBranch = "master";
    };
    ignores = [
      ".directory"
    ];
  };
}

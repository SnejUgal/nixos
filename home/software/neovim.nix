{ pkgs, ... }:

let
  plugins = pkgs.vimPlugins;
in {
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    withRuby = false;
    extraConfig = ''
      set expandtab shiftwidth=4
      autocmd BufRead,BufNewFile *.nix,*.js,*.ts,*.tsx,*.hs,*.lisp set shiftwidth=2

      set number relativenumber numberwidth=6
      set splitright splitbelow
      set langmap=йqцwуeкrеtнyгuшiщoзpх[ъ]фaыsвdаfпgрhоjлkдlж\\;э'яzчxсcмvиbтnьmб\\,ю.ЙQЦWУEКRЕTНYГUШIЩOЗPХ{Ъ}ФAЫSВDАFПGРHОJЛKДLЖ\\:Э\"ЯZЧXСCМVИBТNЬMБ<Ю>ё`Ё~
      set nohlsearch

      set textwidth=80 colorcolumn=81
      autocmd BufRead,BufNewFile *.rs set colorcolumn=101

      tnoremap <Esc> <C-\><C-n>
    '';

    plugins = [
      { plugin = plugins.gruvbox-nvim;
        type = "lua";
        config = ''
          require("gruvbox").setup({
            transparent_mode = true,
            overrides = {
              ["@lsp.type.property"] = { link = "GruvboxFg" },
              ["@lsp.type.parameter"] = { link = "GruvboxFg" },
            },
          })
          vim.cmd("colorscheme gruvbox")
        ''; }
      { plugin = plugins.vim-airline;
        config = ''
          if !exists('g:airline_symbols')
            let g:airline_symbols = {}
          endif
          let g:airline_symbols.linenr = ' '
          let g:airline_symbols.colnr = ':'
          let g:airline_symbols.maxlinenr = '''
          let g:airline_powerline_fonts = 1
          let g:airline_section_z = airline#section#create(['windowswap', '%p%%', 'linenr', 'colnr', 'maxlinenr'])
        ''; }
      { plugin = plugins.nerdtree;
        config = ''
          let NERDTreeNaturalSort = 1
          let NERDTreeChDirMode = 3
          let NERDTreeMouseMode = 3
          let NERDTreeMinimalUI = 1
          let NERDTreeShowHidden = 1
          let NERDTreeIgnore = [
            \ '\.aux$',
            \ '\.bbl$',
            \ '\.bcf',
            \ '\.blg$',
            \ '\.fdb_latexmk$',
            \ '\.fls$',
            \ '\.lof$',
            \ '\.lot$',
            \ '\.run\.xml$',
            \ '\.synctex\.gz$',
            \ '\.toc$'
          \ ]
        ''; }
      plugins.vim-fugitive

      plugins.vim-surround
      plugins.vim-sneak
      plugins.vim-windowswap
      plugins.delimitMate
      { plugin = plugins.vim-better-whitespace;
        config = ''
          let g:better_whitespace_enabled = 1
          let g:strip_whitespace_on_save = 1
          let g:strip_whitelines_at_eof = 1
        ''; }
      { plugin = plugins.indent-blankline-nvim;
        type = "lua";
        config = ''
          require('ibl').setup({
            indent = { char = '▏' },
          })
        ''; }
      { plugin = plugins.comment-nvim;
        type = "lua";
        config = "require('Comment').setup()"; }

      { plugin = plugins.direnv-vim;
        config = "autocmd BufWritePre flake.nix DirenvExport"; }
      { plugin = plugins.vimtex;
        config = ''
          let g:vimtex_compiler_latexmk = {
            \ 'build_dir': ''',
            \ 'options': [
            \   '-shell-escape',
            \   '-verbose',
            \   '-file-line-error',
            \   '-synctex=1',
            \   '-interaction=nonstopmode',
            \ ],
          \ }
        ''; }
      { plugin = plugins.nvim-lspconfig;
        type = "lua";
        config = ''
          local lspconfig = require("lspconfig")

          local snippets = vim.lsp.protocol.make_client_capabilities()
          snippets.textDocument.completion.completionItem.snippetSupport = true

          lspconfig.hls.setup {
            settings = {
              haskell = {
                formattingProvider = "fourmolu"
              }
            }
          }
          lspconfig.pyright.setup {}
          lspconfig.ruff.setup {}
          lspconfig.rust_analyzer.setup {}

          vim.keymap.set("n", "<space>e", vim.diagnostic.open_float)
          vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
          vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
          vim.keymap.set("n", "<space>A", vim.lsp.codelens.run)

          vim.api.nvim_create_autocmd("LspAttach", {
            callback = function(args)
              local opts = { buffer = args.buf }
              vim.keymap.set("n", "<space>d", vim.lsp.buf.definition, opts)
              vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
              vim.keymap.set("n", "<space>K", vim.lsp.buf.hover, opts)
              vim.keymap.set("n", "<space>r", vim.lsp.buf.rename, opts)
              vim.keymap.set("n", "<space>i", vim.lsp.buf.implementation, opts)
              vim.keymap.set("n", "<space>R", vim.lsp.buf.references, opts)
              vim.keymap.set("n", "<space>c", vim.lsp.buf.incoming_calls, opts)
              vim.keymap.set("n", "<space>s", vim.lsp.buf.signature_help, opts)
              vim.keymap.set("n", "<space>l", vim.lsp.buf.workspace_symbol, opts)
              vim.keymap.set({ "n", "v" }, "<space>a", vim.lsp.buf.code_action, opts)
            end
          })

          vim.api.nvim_create_autocmd("BufWritePre", {
            pattern = { "*.rs", "*.hs", "*.py" },
            callback = function(args)
              vim.lsp.buf.format({ async = false })
            end
          })

          vim.api.nvim_create_autocmd({ "BufEnter", "CursorHold", "InsertLeave" }, {
            pattern = { ".rs", "*.hs" },
            callback = function(args)
              vim.lsp.codelens.refresh()
            end
          })
        ''; }

      plugins.vim-pug
    ];
  };
}

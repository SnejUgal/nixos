{ ... }:

{
  programs.thunderbird = {
    enable = true;
    profiles.snejugal = {
      isDefault = true;
      # TODO: make use of https://github.com/nix-community/home-manager/pull/5613
      settings = {
        "mail.accountmanager.accounts" =
        "account_d19a15c70437ed6ea0e4bd1bce2cb335edaf30e47c44bb1b195937dda84ce65f,account_35d12a3b3632e1e7ebc910c9ca28ebb97ee13120fcfd9ec3b4087a3f2c503f9e,account_d79293894fd5c6a1407a1bada9b85fdd36748f0d6519678804d23bad646b6856,account_a906a37b5e5611cef83a3eda5946cc96372bb49258a85e601bc68378ff2f0aa2,account_2e6e018516f4caaf129ac71a377fc6f41a80415230228b697c9be0854ba1f820,account_c1076eecac8c58a7f99af66cbf988288aa1a63b45aa64e5023a32c69b7554722,account_087590740007381a8d622a87f58d417a2d930a207224b28d28ff7d0f88a7fb53,account_dc2c2e517ead472fb97dac74b3cc40e3f97c3bdafc88ea5ce87f04b5b9ce974e,account_a4a4f23682841474462636ce6d22bcd1438d08b565626874a7d23af1f2ae98cd,account_rss,account62";
        "mail.account.account_rss.server" = "server_rss";
        "mail.server.server_rss.name" = "RSS";
        "mail.server.server_rss.type" = "rss";
        "mail.server.server_rss.directory" = ".thunderbird/snejugal/Mail/Feeds-3bcf9f2bf07261cf5b1c7b72e8b006666acb662b93c7a572ef352ed9edb21a8b";
        "mail.server.server_rss.directory-rel" = "[ProfD]Mail/Feeds-3bcf9f2bf07261cf5b1c7b72e8b006666acb662b93c7a572ef352ed9edb21a8b";
        "mail.server.server_rss.hostname" = "Feeds-3bcf9f2bf07261cf5b1c7b72e8b006666acb662b93c7a572ef352ed9edb21a8b";
      };
    };
  };
}

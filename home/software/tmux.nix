{ pkgs, ... }:

{
  programs.tmux = {
    enable = true;
    mouse = true;
    baseIndex = 1;
    escapeTime = 0;
    extraConfig = ''
      bind-key c new-window -c "#{pane_current_path}"
      bind-key % split-window -h -c "#{pane_current_path}"
      bind-key '"' split-window -v -c "#{pane_current_path}"
    '';

    plugins = [
      pkgs.tmuxPlugins.gruvbox

      { plugin = pkgs.tmuxPlugins.battery;
        extraConfig = ''
          set-option -g @batt_remain_short true
          set-option -g status-right "#[bg=colour237,fg=colour239 nobold, nounderscore, noitalics]#[bg=colour239,fg=colour246] %Y-%m-%d  %H:%M #[bg=colour239,fg=colour248,nobold,noitalics,nounderscore]#[bg=colour248,fg=colour237] #{battery_percentage}  #{battery_remain} "
        ''; }
    ];
  };
}

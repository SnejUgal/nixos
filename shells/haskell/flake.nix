{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs { inherit system; };
      hls = pkgs.haskell-language-server.override {
        supportedGhcVersions = [ "98" ];
      };
    in {
      devShells.default = pkgs.mkShell {
        packages = [
          pkgs.stack
          hls
        ];
      };
    });
}

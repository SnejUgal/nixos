{ pkgs, ... }:

{
  hardware.bluetooth.enable = true;

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware.openrazer = {
    enable = true;
    devicesOffOnScreensaver = false;
    syncEffectsEnabled = false;
    batteryNotifier.enable = false;
  };

  systemd.services."battery-charge-threshold" = {
    wantedBy = ["multi-user.target"];
    startLimitBurst = 0;
    serviceConfig.Restart = "on-failure";

    script = ''
      echo 85 > /sys/class/power_supply/BAT0/charge_control_end_threshold
    '';
  };

  services.printing = {
    enable = true;
    drivers = [
      pkgs.cups-kyodialog
    ];
  };
}

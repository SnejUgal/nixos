{ pkgs, ... }:

{
  networking.networkmanager.enable = true;

  networking.firewall = {
    allowedTCPPorts = [ 1234 ];
    logReversePathDrops = true;
    # Pass WireGuard traffic through
    checkReversePath = false;
  };

  # Fix connection to UniversityStudent
  systemd.services.wpa_supplicant.environment.OPENSSL_CONF =
    pkgs.writeText "openssl.cnf" ''
      openssl_conf = openssl_init
      [openssl_init]
      ssl_conf = ssl_sect
      [ssl_sect]
      system_default = system_default_sect
      [system_default_sect]
      Options = UnsafeLegacyRenegotiation
      [system_default_sect]
      CipherString = Default:@SECLEVEL=0
    '';

  services.resolved = {
    enable = true;
    extraConfig = ''
      ResolveUnicastSingleLabel=true
    '';
  };
  networking.networkmanager.dns = "systemd-resolved";
  environment.etc."resolv.conf".mode = "direct-symlink";
}

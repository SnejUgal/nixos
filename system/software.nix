{ pkgs, lib, ... }:

{
  # todo: Wayland-only 🦄
  # Currently, these programs have to be run under XWayland:
  # - GIMP
  # - Krita
  # - Rustdesk (cannot type on the remote machine if host is on Wayland)
  # - VLC prefers X over Wayland for some reason
  # - Samsung Galaxy Buds
  # - Polychromatic
  services.xserver = {
    enable = true;
    excludePackages = with pkgs; with xorg; [
      xrandr
      xrdb
      setxkbmap
      xlsclients
      xset
      xsetroot
      xinput
      xprop
      xauth
      xterm
    ];
  };

  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
  };

  services.desktopManager.plasma6.enable = true;
  programs.kdeconnect = {
    enable = true;
    package = lib.mkForce pkgs.kdePackages.kdeconnect-kde;
  };
  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    plasma-browser-integration
    konsole
    elisa
    gwenview
    okular
    kate
    khelpcenter
    krdp
  ];

  environment.systemPackages = with pkgs; [
    unixtools.xxd
    ripgrep
    bat
    fd
    du-dust
    file
    tealdeer

    openrazer-daemon
  ];

  services.fwupd.enable = true;
  programs = {
    iotop.enable = true;
    htop.enable = true;
    nano.enable = false;
    neovim = {
      enable = true;
      withRuby = false;
      withPython3 = false;
    };

    partition-manager.enable = true;
  };

  virtualisation = {
    docker.rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };

  programs.steam.enable = true;

  services.netbird.enable = true;

  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "steam"
      "steam-unwrapped"
      "steam-run"
      "cups-kyodialog"
    ];
}
